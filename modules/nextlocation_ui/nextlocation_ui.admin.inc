<?php

/**
 * @file
 * Provide admin forms.
 */

function nextlocation_admin() {
    $output = '';
    
    //Display all entities
    $entities = array_values(nextlocation_load_multiple(FALSE));
    $output .= nextlocation_display($entities,'table');
    
    return $output;
}

/**
* Generates the nextlocation editing form.
*/
function nextlocation_form($form, &$form_state, $nextlocation, $op = 'edit') {

  if ($op == 'clone') {
    $nextlocation->location .= ' (cloned)';
  }

  $form['location'] = array(
    '#title' => t('Location title'),
    '#type' => 'textfield',
    '#default_value' => $nextlocation->location,
  );
  $form['latitude'] = array(
    '#title' => t('Latitude'),
    '#type' => 'textfield',
    '#default_value' => $nextlocation->latitude,
  );
  $form['longitude'] = array(
    '#title' => t('Longitude'),
    '#type' => 'textfield',
    '#default_value' => $nextlocation->longitude,
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save nextlocation'),
    '#weight' => 40,
  );
  return $form;
}

/**
* Form API submit callback for the type form.
*/
function nextlocation_form_submit(&$form, &$form_state) {
  $nextlocation = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $nextlocation->save();
  $form_state['redirect'] = 'admin/content/nextlocation';
}
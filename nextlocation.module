<?php

/**
 * @file
 * Provide API with Entity API for managing locations.
 */
/**
 * Implements hook_permission()
 */
function nextlocation_permission() {
  $perms = array();
  
  $perms['administer nextlocation api'] = array(
    'title' => t('Administer NextLocation API'), 
    'description' => t('Perform administration tasks for NextLocation API, like NextLocation fields.'),
  );
  $perms['administer nextlocation'] = array(
    'title' => t('Administer NextLocation'), 
    'description' => t('Perform administration tasks for NextLocations.'),
  );
  $perms['view nextlocation'] = array(
    'title' => t('View a NextLocation'), 
    'description' => t('View a NextLocation page.'),
  );
  $perms['add nextlocation'] = array(
    'title' => t('Add a NextLocation'), 
    'description' => t('Add a NextLocation location.'),
  );
  $perms['edit nextlocation'] = array(
    'title' => t('Edit a NextLocation'), 
    'description' => t('Edit a NextLocation location.'),
  );
  $perms['delete nextlocation'] = array(
    'title' => t('Delete a NextLocation'), 
    'description' => t('Delete a NextLocation location.'),
  );
  
  return $perms;
}

/**
 * Implements hook_menu()
 */
function nextlocation_menu() {
	$items = array();
	
	/*$items['homes/colorbox/%node'] = array(
		'title callback' => 'node_page_title',
		'title arguments' => array(2),
		'page callback' => 'homes_colorbox_node',
		'page arguments' => array(2),
		'access callback' => 'node_access',
		'access arguments' => array('view',2),
		'type' => MENU_NORMAL_ITEM,
	);*/
	$items['test-nextlocation'] = array(
    'title' => t('NextLocation Test Page'),
		'page callback' => 'nextlocation_test_page',
		'access arguments' => array('access content'),
		'type' => MENU_NORMAL_ITEM,
	);
	$items['admin/config/location/apiconfig'] = array(
    'title' => t('NextLocation API'),
		'page callback' => 'nextlocation_configure',
		'access arguments' => array('administer nextlocation api'),
		'type' => MENU_NORMAL_ITEM,
	);
	/*$items['admin/dx'] = array(
		'title' => 'dX',
		'page callback' => 'dx_list',
		'access arguments' => array('administer dx'),
		'type' => MENU_NORMAL_ITEM,
	);
	$items['admin/dx/list'] = array(
		'title' => 'Liste des tables',
		'type' => MENU_DEFAULT_LOCAL_TASK,
		'weight' => -8,
	);
	$items['admin/dx/add'] = array(
		'title' => 'Ajouter une table',
		'type' => MENU_LOCAL_TASK,
		'page callback' => 'dx_add',
		'access arguments' => array('administer dx'),
		'weight' => -6,
	);*/
	
	return $items;
}

/**
 * Implement hook_entity_info().
 */
function nextlocation_entity_info() {
  $return = array(
    'nextlocation' => array(
      'label' => t('NextLocation'),
      'entity class' => 'NextLocationClass',
      'controller class' => 'EntityAPIController',
      'base table' => 'nextlocation',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'nlid',
        'name' => 'location',
      ),
      // Make use the class' label() and uri() implementation by default.
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
    ),
  );
  return $return;
}

/**
 * Load multiple nextlocations based on certain conditions.
 *
 * @param $nlids
 *   An array of entity IDs.
 * @param $conditions
 *   An array of conditions to match against the {entity} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of nextlocation objects, indexed by nlid.
 */
function nextlocation_load_multiple($nlids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('nextlocation', $nlids, $conditions, $reset);
}

/**
 * Delete multiple nextlocations.
 *
 * @param $nlids
 *   An array of nextlocation IDs.
 */
function nextlocation_delete_multiple(array $nlids) {
  entity_get_controller('nextlocation')->delete($nlids);
}


/**
 * Main class for nextlocations.
 */
class NextLocationClass extends Entity {

  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, 'nextlocation');
  }

  /**
   * Override buildContent() to add the username to the output.
   */
  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $content = entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, array());
    //$content = array();
    $content['latlong'] = array(
      '#markup' => "Lat, Long : [". $this->latitude . ', ' . $this->longitude . ']',
    );
    return $content;
  }

  /**
   * Specifies the identifier, which is name by default.
   */
  public function identifier() {
    return isset($this->{$this->idKey}) ? $this->{$this->idKey} : NULL;
  }
  
  /**
   * Specifies the default label, which is picked up by label() by default.
   */
  protected function defaultLabel() {
    return isset($this->location) ? $this->location : NULL;
  }

  /**
   * Specifies the default uri, which is picked up by uri() by default.
   */
  protected function defaultURI() {
    return array('path' => 'nextlocation/' . $this->identifier());
  }
}



/**
 * More specific functions :
 * 
 * Provide a custom-way to CRUD functions for NextLocations
 *  - nextlocation_load($nlid[, $conditions[, $reset]) : retrieve one or more NextLocations objects
 *  - nextlocation_delete($nlid[, $conditions[, $reset]) : delete one or more NextLocations objects
 *  - nextlocation_create($objects[, $save]) : one function for create one or more NextLocation, saved by default in database
 */


/**
 * Load one or more nextlocation based on certain conditions.
 *
 * @param $nlid
 *   A NextLocation ID or an array of NextLocation IDs or a location name of a NextLocation.
 * @param $conditions
 *   An array of conditions to match against the {entity} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of nextlocation objects, indexed by nlid.
 */
function nextlocation_load($nlid, $conditions = array(), $reset = FALSE) {
  if(is_array($nlid))
    return nextlocation_load_multiple($nlid, $conditions, $reset);
  elseif(is_string($nlid)) {
    $tmp = nextlocation_load_multiple(FALSE, array_merge(array('location'=>$nlid),$conditions), $reset);
    return (is_array($tmp) && count($tmp) == 1) ? array_pop($tmp) : $tmp;
  }
  else {
    $tmp = nextlocation_load_multiple(array($nlid), $conditions, $reset);
    return (is_array($tmp) && isset($tmp[$nlid])) ? $tmp[$nlid] : null;
  }
}

/**
 * Delete one or more nextlocations.
 *
 * @param $nlid
 *   An array of nextlocation IDs.
 */
function nextlocation_delete($nlid) {
  if(is_array($nlid))
    nextlocation_delete_multiple($nlid);
  else
    nextlocation_delete_multiple(array($nlid));
}

/**
 * Create one or more nextlocations.
 *
 * @param $objects
 *   An array of objects, or one object.
 * @param $save = true
 *   Specify if the nextlocations created must be saved.
 * @return
 *   An array of nextlocation objects, indexed by nlid, or one NextLocationClass entity.
 */
function nextlocation_create($objects, $save = true) {
  //If we try to create one NextLocation
  if(is_object($objects)) {
    $entity = null;
    if(property_exists($objects,'location'))
      $entity = entity_create('nextlocation',get_object_vars($objects));
    if($entity && $save)
      $entity->save();
    return $entity;
  }
  //If we try to create many NextLocations at once
  elseif(is_array($objects)) {
    $entities = array();
    foreach($objects as $object) {
      $tmp = null;
      if(property_exists($object,'location'))
        $tmp = entity_create('nextlocation',get_object_vars($object));
      if($tmp) {
        if($save) {
          $tmp->save();
          if(property_exists($tmp,'nlid'))
            $entities[$tmp->nlid] = $tmp;
        }
        else
          $entities[] = $tmp;
      }
    }
    return $entities;
  }
}

/**
 * Update one or more objects.
 *
 * @param $objects
 *   An array of NextLocations objects or simple objects.
 * @return
 *   Number of updated NextLocation entities
 */
function nextlocation_update($objects) {
  $nb_updated = 0;
  //If we try to update one NextLocation
  if(is_object($objects)) {
    if(get_class($objects) == 'NextLocationClass') {
      $objects->save();
      $nb_updated++;
    }
    else {
      $entity = null;
      if(property_exists($objects,'nlid')) {
        $entity = nextlocation_load($objects->nlid);
        $updated = false;
        if($entity) {
          foreach($objects as $pptid => $pptval) {
            if(property_exists($entity,$pptid) && $entity->{$pptid} != $pptval) {
              $entity->{$pptid} = $pptval;
              $updated = true;
            }
          }
        }
        if($entity && $updated) {
          $entity->save();
          $nb_updated++;
        }
      }
    }
  }
  //If we try to create many NextLocations at once
  elseif(is_array($objects)) {
    foreach($objects as $object) {
      if(get_class($object) == 'NextLocationClass') {
        $object->save();
        $nb_updated++;
      }
      else {
        $entity = null;
        if(property_exists($object,'nlid')) {
          $entity = nextlocation_load($object->nlid);
          $updated = false;
          if($entity) {
            foreach($object as $pptid => $pptval) {
              if(property_exists($entity,$pptid) && $entity->{$pptid} != $pptval) {
                $entity->{$pptid} = $pptval;
                $updated = true;
              }
            }
          }
          if($entity && $updated) {
            $entity->save();
            $nb_updated++;
          }
        }
      }
    }
  }
  return $nb_updated;
}




/**
 * PAGES
 */


function nextlocation_configure() {
  $output = '';
  
  $output .= '<h2>Configuration</h2>';
  
  return $output;
}


function nextlocation_test_page() {
    $output = '';
    
    $newentity = new stdClass();
    $newentity->nlid = 9;
    $newentity->location = 'Albi Madeleine';
    $newentity->latitude = 29.747421;
    $newentity->longitude = 32.589477;
    $newentity2 = nextlocation_load(10);
    $newentity2->location = 'Chemin des Cougourles';
    $newentity2->latitude = 29.747421;
    $newentity2->longitude = 12.589477;
    
    //Creation example of one nextlocation
    /*$entity = nextlocation_create($newentity);
    $output .= '<pre>'.print_r($entity,true).'</pre>';
    $output .= '<pre>'.print_r($entity->nlid,true).'</pre>';*/
    
    //Creation example of many nextlocations at once
    /*$entities = nextlocation_create(array($newentity,$newentity2));
    $output .= '<pre>'.print_r($entities,true).'</pre>';*/
    
    //Deletion example of one nextlocation
    //nextlocation_delete(19);
    
    //Deletion example of many nextlocations at once
    //nextlocation_delete(array(20,21,22));
    
    //Update example of one nextlocation
    //$output .= '<pre>'.print_r($newentity2,true).'</pre>';
    //$output .= '<br />'.nextlocation_update($newentity2).'<br />';
    
    //Update example of many nextlocations at once
    //$output .= '<br />'.nextlocation_update(array($newentity, $newentity2));
    
    $entities = array_values(nextlocation_load_multiple(FALSE));
    
    //$output .= '<pre>'.print_r($entities,true).'</pre>';
    
    //Render example of an array of nextlocations
    foreach($entities as $nlid => $nl) {
      $output .= '<pre>'.print_r(entity_label('nextlocation',$nl).' [nlid:'.$nl->nlid.']',true).'</pre>';
      //$output .= '<pre>'.print_r(entity_build_content('nextlocation',$nl),true).'</pre>';
      $render = $nl->view();
      $output .= drupal_render($render);
    }
    //$output .= '<pre>'.print_r(nextlocation_load(12),true).'</pre>';
    //$output .= '<pre>'.print_r($_SESSION,true).'</pre>';
    
    return $output;
}

